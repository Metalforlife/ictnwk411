#!/bin/sh

# build1.sh
# Set up Cloud9 workstation (Amazon Linux 2)

sudo yum update -y
sudo yum install ansible -y
mkdir /home/ec2-user/bin

echo '"alias ls=ls -al"' >> /home/ec2-user/.bash_profile

echo 'set number' >> /home/ec2-user/.vimrc
echo 'syntax on' >> /home/ec2-user/.vimrc
echo 'set tabstop=2' >> /home/ec2-user/.vimrc

ssh-keygen -t rsa -b 4096 -n "" -f ~/.ssh/id_rsa
