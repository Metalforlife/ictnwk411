#!/bin/sh

SECGROUPID="sg-0c7b571fa160498fd"
KEYNAME="jvallerant96"
NAME="jvallerant"
USERDATASCRIPT="/home/ec2-user/environment/ictnwk411/build-nginx.sh"

######################

AMIID="ami-02354e95b39ca8dec"

aws ec2 run-instances \
    --count 1 \
    --image-id $AMIID \
    --instance-type t2.micro \
    --key-name $KEYNAME \
    --associate-public-ip-address \
    --security-group-ids $SECGROUPID \
    --user-data file://$USERDATASCRIPT \
    --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$NAME}]"

