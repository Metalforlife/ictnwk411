#!/bin/sh

aws s3 sync ~/bin s3://jvallerant-backup2/bin
aws s3 sync ~/environment s3://jvallerant-backup2/environment
aws s3 sync ~/.ssh s3://jvallerant-backup2/.ssh
aws s3 cp ~/.vimrc s3://jvallerant-backup2
aws s3 cp ~/.bash_profile s3://jvallerant-backup
aws s3 cp ~/.bash_profile s3://jvallerant-backup2
