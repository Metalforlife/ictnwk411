#!/bin/sh

# Install software
yum -y update
amazon-linux-extras install nginx1 -y
yum -y install git

# Change location to web server docroot directory
cd /usr/share/nginx/html/

# Create an archive directory and move all existing files into it
mkdir archive
mv * archive

# Download website files from internet location
wget https://github.com/StartBootstrap/startbootstrap-agency/archive/gh-pages.zip

# Unzip website files
unzip gh-pages.zip

# Copy website files from unzipped directory to /usr/share/nginx/html
mv startbootstrap-agency-gh-pages/* .

# Clean up
mv startbootstrap-agency-gh-pages* archive
mv gh-pages.zip archive

# Enable and start web server
systemctl enable nginx
systemctl start nginx

